# easy_zip_backup_rust

Программа для простого создания бэкапов на локальном компьютере.

## Первоначальная настройка

Основная часть настроек находится в файле config.toml
- d-format - формат имени ежедневного бэкапа
- w-format - формат имени еженедельного бэкапа
- m-format - формат имени ежемесячного бэкапа
- directory - директория, куда будут сохраняться бэкапы.
- log_l - уровень логирования, но пока-что параметр практически не используется
- weekday - день недели, в который создаётся бэкап, где 1- понедельник, 2-вторник и т.д.
- monthday - день в месяце, когда делается бэкап
- temp_dir - дериктория, в которую сохраняются архивы и другая инфомрация, которая пойдёт в бэкап
- win_c - путь к файлу скрипта для бэкапов (для windows)
- nix_c = путь к файлу скрипта для бэкапов (для Linux)

Очень важная часть - это настройка скриптов win.bat и linux.sh. Эти скрипты должны формировать папку "temp_dir", которая потом пойдёт в бэкап.

## Пример файла linux.sh

```
rm to_back_up/* -vrf
PGPASSWORD= <password>
export PGPASSWORD
tar -cvf to_back_up/a.tar <path>
pg_dump -U <user name> <base name> > to_back_up/base_mydb1.sql
unset PGPASSWORD
```
Этот скрипт создаёт бэкап базы **postgresql** и папки с путём **path**.

Проект собирается на стабильной версии rust коммандой **cargo build --release**
