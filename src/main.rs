extern crate time;

extern crate toml;
extern crate serde;
//STD
use std::fs;
use std::fs::{File, copy};
use std::io::prelude::*;
use std::process::Command;
//Library from extern crates
use time::{OffsetDateTime};
use serde_derive::Deserialize;
///Структура для конфига /Struct for config
#[derive(Deserialize)] 
struct Config {
    d_format: String,
    w_format: String,
    m_format: String,
    directory: String,
    log_l: i32,
    backup_command: CommandS,
    weekday: u8,
    monthday: u8,
    temp_dir: String
}
///Структура для конфига /Struct for config
#[derive(Deserialize)] 
struct CommandS {
    win_c: String,
    nix_c: String
}
fn main() {
    let mut file = match File::open("config.toml") {
        Ok(data) => data,
        Err(_)   => File::open("/etc/easy_backup.toml").unwrap()
    };
    let mut content = String::new();

    file.read_to_string(&mut content).unwrap();
    let config: Config = toml::from_str(content.as_str()).unwrap();
   
    let dir = std::fs::read_dir(format!("{}/day/{}",config.directory, OffsetDateTime::now_utc().format(config.d_format.clone())));
   
    if !(dir.is_ok()){
        let command_r = set_command(&config);
        if config.log_l >= 2 {
            println!("{}", command_r);
        }

        print!("Create backup",);
        backup_day(&config);
        backup_week(&config);
        backup_month(&config);
    }
    else{
        println!("Nothing to backup")
    }
    println!("{}",OffsetDateTime::now_utc().format("%c"));
    clear_folder(&config);

}
/// Функция для удаления старых бэкапов /Function for remove old backups
fn clear_folder(config: &Config){ 
    let dir1 = std::fs::read_dir(format!("{}/day/", config.directory)).unwrap();
    let mut buf_vec1 = Vec::new();
    for i in dir1{      
        let el = i.unwrap().path();      
        buf_vec1.push(el);
    }
    while buf_vec1.len()>6{
        let buf_path = buf_vec1[0].clone();
        println!("УДАЛЯЮ {:?}",buf_path);
        std::fs::remove_dir_all(buf_path).unwrap();
        buf_vec1.remove(0);
    }
}
///Функция для исполнения комманды / Function for execute command
fn set_command(config: &Config) -> std::process::ExitStatus{ 
    let output = if cfg!(target_os = "windows") {
        let command = config.backup_command.win_c.clone();
        println!("{}",command);
        Command::new(format!("{}", command))
            .status()
            .unwrap()
    }
    else{
        let command = config.backup_command.nix_c.clone();
        println!("{}",command);
        Command::new("sh")
            .arg("-c")
            .arg(format!("{}", command))
            .status()
            .unwrap()
    }; 
    output
}
///Функция для бэкапов / Function for create backup
fn backup(prifix: String, postfix: String, config: &Config){
    let buf_patch = format!("{}/{}", prifix, postfix);
    fs::create_dir_all(buf_patch.clone()).unwrap();
    for i in std::fs::read_dir(config.temp_dir.clone()).unwrap(){
        println!("{:?}", i);
        println!("{:?}", buf_patch);
        let i = i.unwrap();
        let file_name = i.file_name().into_string().unwrap();
        //print!("FILE NAME = {}", file_name);
        let from_t = i.path();
        let to_t = format!("{}/{}",buf_patch,file_name);
        //print!("FROM {:?} ", from_t);
        //println!("TO {:?}", to_t);
        copy(from_t,
             to_t).unwrap();
    }
}
/// Функция для создания бэкапов дня / Function for create dayly backup
fn backup_day(config: &Config){ 
    backup(format!("{}/day", config.directory).to_string(), OffsetDateTime::now_utc().format(config.d_format.clone()), &config);
    println!("Dayly backup done!");
    //format("%Y%m%d"));
}
/// Функция для создания бэкапов недели / Function for create weekly backup
fn backup_week(config: &Config){
    if OffsetDateTime::now_utc().weekday().number_from_monday() == config.weekday{
        backup(format!("{}/week", config.directory).to_string(), OffsetDateTime::now_utc().format(config.w_format.clone()), &config);
        println!("Weekly backup done!");
    }
}
/// Функция для создания бэкапов месяца/ Function for create monthly backup
fn backup_month(config: &Config){
    if OffsetDateTime::now_utc().day() == config.monthday {
        backup(format!("{}/month", config.directory).to_string(), OffsetDateTime::now_utc().format(config.m_format.clone()), &config);
        println!("Monthly backup done!");
    }
}
